using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace SMECService.Hubs
{
   
    public interface IdataHub
    {
              Task Add(string value);

              Task Delete(string value);

              Task Update(string value);
    }

    public class DataHub : Hub<IdataHub>
    {
       
        public async Task Add(string value) => await Clients.All.Add(value);

        public async Task Delete(string value) => await Clients.All.Delete(value);

        public async Task Update(string value) => await Clients.All.Update(value);
    }
}