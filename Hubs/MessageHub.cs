using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
 
namespace SMECService.Hubs
{
    public class MessageHub : Hub
    {
        public Task Send(string message)
        {
            return Clients.All.SendAsync("Send", message);
        }
    }
}