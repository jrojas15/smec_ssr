
## [4.0.2] - 11/12/2018 [Unreleased]

### Added
- Material design lite data-table scss only.

### Changed
- `angular.json` ya no incorpora los scripts y css de material desing litle. Solo los necesarios para data-table.

### Removed
- flex-box.
- signal-r service.
- Material design lite.
- `index.html` se ha eliminado la etiqueta script de signal-r.
- se ha eliminado el paquete aspnet/signal-r y todas sus referencias.

### Notes
- dotnet publish clientApp tamaño: `34.5 MB`.
- ng serve --prod --aot --build-optimizer clientApp tamaño: `7,55 MB`.

## [4.0.1] - 10/12/2018 [Released]

### Added

- ChartComponent: `ScrollbarY` zoom vertical.
- ChartComponent: características resposive, `this.chart.responsive`.
- ChartComponent: se muestra `snackBar` al guardar o eliminar series del gráfico.
- ChartComponent: evento `dateAxis.zoom` para zoom inicial.

### Changed

- ChartComponent: se ha cambiado las dimensiones del gráfico.
- ChartComponent: el gráfico no se vuelve a cargar cuando se añade/elimina/guarda series.

### Fixed

- ChartComponent: la descripción del foco se cambiaba cuando no debia.
- ChartComponent: cuando se borraba un contaminante del gráfico(No BD), no se  actualizaba el array que almacena las series.
- ChartComponent: Al cambiar de ruta y volver si no había datos para mostrar   el contenedor del gráfico se mostraba en lugar de `DisplayMessagesComponent`.

### Removed

- `CacheRouteReuseStrategy`.

### Packages Updates

- Versión de amcharts actualizada a la `4.0.7`.