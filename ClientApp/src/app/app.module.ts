/** Modules */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import 'polyfills';
import { FilterPipePipe } from '@pipes/filter-pipe.pipe';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HistoricalMaterialModule } from '@shared/historicalShared/historical-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';
import { LoginModule } from './shared/LoginSharedModule/login.module';
/** Services */
import { AuthService } from '@services/auth.service';
import { AuthGuard } from '@services/auth-guard.service';
import { RoleGuardService } from '@services/role-guard.service';
/** Components */
import { AppComponent } from './app.component';
import { HistoricalComponent } from '@shared/historicalShared/historical/historical-component.component';
import { SpeedDialFabComponent } from '@shared/speed-dial-fab/speed-dial-fab.component';
// import { LoginViewComponent } from '@shared/loginShared/login-view/login-view.component';
/** Cache Route Strategy */
import { CacheRouteReuseStrategy } from './cache-route-reuse.strategy';
import { RouteReuseStrategy } from '@angular/router';
/** Interceptors */
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiInterceptor } from './core/url.constants';
import { AuthInterceptor } from '@services/auth.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    SpeedDialFabComponent,
    HistoricalComponent,
    FilterPipePipe,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HistoricalMaterialModule,
    AppMaterialModule,
    AppRoutingModule,
    LoginModule,
  ],
  entryComponents: [
    HistoricalComponent,
  ],
  providers: [
    // { provide: RouteReuseStrategy, useClass: CacheRouteReuseStrategy },
    // { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    AuthService,
    AuthGuard,
    RoleGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
