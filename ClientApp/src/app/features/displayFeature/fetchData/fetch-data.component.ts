import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
// Models
import { Focus } from '@models/models';
// Services
import { FocusService } from '@services/api.focus.service';
// import { SignalRService } from '@services/signal-r.service';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html',
  styleUrls: ['./fetch-data.component.scss'],
  // providers: [SignalRService]
})
export class FetchDataComponent implements OnInit, OnDestroy {
  title = '¡Atención!';
  public focus_list = [];
  private ngUnsubscribe = new Subject();
  displayedColumns = ['analyzer', 'description'];
  interval;
  IsConnected = true;
  message = 'No hay datos disponibles';

  constructor(
    private _focusService: FocusService,
  ) {

  }
  ngOnInit(): void {
    Promise.resolve().then(_ => {
      this.CargarFocos();
    }).then(_ => {
      this.refreshData();
    });

  }

  trackByFn(index, item) {
    return item.id; // unique id corresponding to the item
  }

  refreshData() {
    this.interval = setInterval(() => {
      this.CargarFocos();
    }, 10000);

  }

  CargarFocos() {
    this._focusService.focus_GetAll()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => {
        this.focus_list = res.filter(x => x.inService === 1);
        this.IsConnected = true;
        // this.focus_list = res.filter(f => f.analyzers.filter(a => a.sensors.length > 0));
      }, (error: HttpErrorResponse) => {
        // console.log('Error conn con el servidor. Reconectando ...', error.name, error.message);
        this.IsConnected = false;
      });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    clearInterval(this.interval);
  }
}
