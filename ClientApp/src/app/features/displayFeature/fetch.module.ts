import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FetchRoutingModule } from './fetch-routing.module';
import { FetchDataComponent } from './fetchData/fetch-data.component';
import { SensorRealTimeDataComponent } from './sensor-rt-data/sensor-rt-data.component';
import { FetchMaterialModule } from './fetch-material.module';

import { FocusService } from '@services/api.focus.service';
import { CurrentAnalogDataService } from '@services/api.currentAnalogData.service';

import { DisplayMessagesModule } from '@shared/display-messages/display-messages.module';

@NgModule({
  imports: [
    CommonModule,
    FetchRoutingModule,
    FetchMaterialModule,
    DisplayMessagesModule
  ],
  declarations: [
    FetchDataComponent,
    SensorRealTimeDataComponent
  ],
  providers: [
    FocusService,
    CurrentAnalogDataService
  ]
})
export class FetchModule { }
