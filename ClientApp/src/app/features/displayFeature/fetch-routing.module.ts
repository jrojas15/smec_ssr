import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FetchDataComponent } from './fetchData/fetch-data.component';

const appRoutes: Routes = [{
  path: '',
  component: FetchDataComponent,
   data: { shouldDetach: false }
}];
@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class FetchRoutingModule { }
