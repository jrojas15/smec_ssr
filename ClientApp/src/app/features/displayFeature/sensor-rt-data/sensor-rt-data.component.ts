/* tslint:disable */
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// Models
import { ICurrentAnalogData } from '@models/models';
// Services
import { CurrentAnalogDataService } from '@services/api.currentAnalogData.service';

@Component({
  selector: '[app-sensor-rt-data]',
  templateUrl: './sensor-rt-data.component.html',
  styleUrls: ['./sensor-rt-data.component.scss']
})
export class SensorRealTimeDataComponent implements OnInit, OnDestroy {
  // @Input() sensor: Sensor;
  @Input('app-sensor-rt-data') sensor;
  interval: any;
  public currentAnalogData: ICurrentAnalogData;
  private ngUnsubscribe = new Subject();

  constructor(
    private currentSensorDataService: CurrentAnalogDataService,
  ) {
  }

  refreshData() {
    this.currentSensorDataService.sensor_GetCurrentAnalogData(this.sensor.sensorId)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((currentAnalogData: any) => {
        this.currentAnalogData = currentAnalogData;
      }, (error: HttpErrorResponse) => {
        // console.log('sensor-rt-data: ', error.name + ' ' + error.message);
      });
  }

  ngOnInit(): void {
    this.refreshData();
    this.interval = setInterval(() => {
      this.refreshData();
    }, 10000);
  }


  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    clearInterval(this.interval);
  }
}

