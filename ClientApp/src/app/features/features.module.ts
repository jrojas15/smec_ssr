import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FeaturesRoutingModule } from './features-routing.module';
import { FeaturesMaterialModule } from './features-material.module';

import { DataService } from '@services/data.service';
import { FocusService } from '@services/api.focus.service';
import { AnalyzerService } from '@services/api.analyzer.service';
import { SensorService } from '@services/api.sensor.service';
import { MeasuringComponentService } from '@services/api.measuringComponent.service';
import { UnitService } from '@services/api.unit.service';
import { PerifericService } from '@services/api.periferic.service';
import { CalibrationFunctionService } from '@services/api.calibrationFunction.service';
import { DownloadService } from '@services/download.service';


import { FeatureRootComponentComponent } from './feature-root-component/feature-root-component.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FeaturesMaterialModule,
        FeaturesRoutingModule,
    ],
    declarations: [FeatureRootComponentComponent],
    providers: [
        DataService,
        FocusService,
        AnalyzerService,
        SensorService,
        MeasuringComponentService,
        UnitService,
        PerifericService,
        CalibrationFunctionService,
        DownloadService
    ]
})
export class FeaturesModule { }
