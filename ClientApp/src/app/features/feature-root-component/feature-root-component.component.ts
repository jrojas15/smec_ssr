import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router, Event, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
// Components
// Services
import { AuthService } from '@services/auth.service';
import { CacheRouteReuseStrategy } from '../../cache-route-reuse.strategy';

@Component({
  selector: 'app-feature-root-component',
  templateUrl: './feature-root-component.component.html',
  styleUrls: ['./feature-root-component.component.scss'],
  providers: [CacheRouteReuseStrategy]
})
export class FeatureRootComponentComponent implements OnInit {
  title: string;
  routeData;
  url;
  routerEvent;
  isExpanded = true;
  opened: boolean;
  username: string;
  showLoadingIndicator = true;
  navigationList = [];
  constructor(
    public authService: AuthService,
    public dialog: MatDialog,
    private router: Router,
    private cacheResue: CacheRouteReuseStrategy,

  ) {

    this.router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.showLoadingIndicator = true;
      }

      if (routerEvent instanceof NavigationEnd || routerEvent instanceof NavigationCancel || routerEvent instanceof NavigationError) {
        setTimeout(() => {
          this.showLoadingIndicator = false;
        }, 2000);
      }
    });
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      //  console.log(localStorage.getItem('currentUser'));
      this.username = localStorage.getItem('currentUser');

      this.router.navigate(['features']);
    } else {
      this.router.navigate(['login']);
    }
    this.routerEvent = this.router.events.subscribe((data) => {
      const url = this.router.url;
      this.url = url;
      //  console.log('URL =>', this.url);
      if (this.url === '/features/visualizacion') {
        this.title = 'Visualización';
      }
      if (this.url === '/features/focus') {
        this.title = 'Focos de emisiones';
      }
      if (this.url === '/features/calibration') {
        this.title = 'Funciones de calibración';
      }
      if (this.url === ' /features/historical') {
        this.title = 'Históricos';
      }
      if (this.url === '/features/periferic') {
        this.title = 'Periféricos';
      }
      if (this.url === '/features/graficoHistorico') {
        this.title = 'Gráfico históricos';
      }
    });

    this.navigationList = [
      { path: 'visualizacion', active: 'active', icon: 'list', title: 'Visualización' },
      // {path: 'focus', active: 'active', icon: 'looks_two', title: 'Focos' },
      // {path: 'calibration', active: 'active', icon: 'looks_3', title: 'Función de Calibración' },
      // {path: 'periferic', active: 'active', icon: 'looks_4', title: 'Periferic' },
      { path: 'graficoHistorico', active: 'active', icon: 'show_chart', title: 'Gráfico histórico' },
    ];
  }

  logOut() {
    this.authService.logout();
  }

}
