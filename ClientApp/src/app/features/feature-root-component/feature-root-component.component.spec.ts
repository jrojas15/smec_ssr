import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureRootComponentComponent } from './feature-root-component.component';

describe('FeatureRootComponentComponent', () => {
  let component: FeatureRootComponentComponent;
  let fixture: ComponentFixture<FeatureRootComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureRootComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureRootComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
