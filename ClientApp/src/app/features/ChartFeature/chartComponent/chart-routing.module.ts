import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartComponent } from './chart.component';

const appRoutes: Routes = [{
  path: '',
  component: ChartComponent,
  data: { shouldDetach: true }
}];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class ChartRoutingModule { }
