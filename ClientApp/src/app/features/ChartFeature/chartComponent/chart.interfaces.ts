export interface ChartParams {
    id?: number;
    focusId: number;
    sensorId: number;
    description: string;
    measuringName: string;
}

