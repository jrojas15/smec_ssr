/* tslint:disable */
import { Injectable, Inject, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { CurrentAnalogDataService } from '@services/api.currentAnalogData.service';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { Serie } from '@models/models';
import { ChartParams } from './chart.interfaces';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4lang_es_ES from '@amcharts/amcharts4/lang/es_ES';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
// import am4themes_animated from '@amcharts/amcharts4/themes/animated';
// import am4themes_dark from '@amcharts/amcharts4/themes/amchartsdark';
// import { RoleGuardService } from '@services/role-guard.service';

@Injectable()
export class ChartService {
    private http: HttpClient;
    private chartValueProvider: string[] = [];
    public chart: am4charts.XYChart;

    private names: string[];
    private namesSource: BehaviorSubject<Array<string>> = new BehaviorSubject([]);
    public currentNames = this.namesSource.asObservable();

    private message: string;
    private messageSource = new BehaviorSubject(this.message);
    public curretMessage = this.messageSource.asObservable();

    private loading: boolean;
    private loadingSource = new BehaviorSubject(this.loading);
    public isLoading = this.loadingSource.asObservable();

    private connection: boolean;
    private connectionSource = new BehaviorSubject(this.connection);
    public isConnected = this.connectionSource.asObservable();

    public colors: IColors;
    private colorContaminant: IcolorContaminant;

    constructor(
        private _currentanalogDataService: CurrentAnalogDataService,
        // private _roleService: RoleGuardService,
        public snackBar: MatSnackBar,
        private zone: NgZone,
        @Inject(HttpClient) http: HttpClient
    ) {
        this.http = http;
        this.colors = { blue: '#0040ff', red: '#ff0000', green: '#33cc33', orange: '#ff8000' };
        this.colorContaminant = {
            CO2: this.colors.red,
            CH4: this.colors.blue,
            H2S: this.colors.green,
            O2: this.colors.orange
        };
    }


    generateChart(series: any[], initialDate, finalDate) {
        if (this.chart) {
            this.chart.dispose();
        }
        initialDate = moment(initialDate).format('YYYY-MM-DD');
        finalDate = moment(finalDate).format('YYYY-MM-DD');
        this.loadingSource.next(true);
        this.chartValueProvider = [];
        this.namesSource.next([]);
        let arrayResult = [];
        let count = 0;
        for (const serie of series) {
            this.addNames(serie.measuringName);
            this._currentanalogDataService.sensor_GetHistoricalAnalogData(serie.sensorId, initialDate, finalDate)
                .subscribe(res => {
                    this.connectionSource.next(true);
                    const result = <any>res;
                    this.chartValueProvider = result.map(data => {
                        const chartValues = data.value;
                        return chartValues;
                    });
                    res.forEach((sr, index) => {
                        if (!arrayResult[index]) arrayResult[index] = {};
                        arrayResult[index]['date'] = new Date(sr.timeStamp);
                        //if (sr.value !== null || 0) {
                        arrayResult[index][serie.measuringName] = sr.value;
                        //}
                    });
                    count++;
                    if (count === series.length) {
                        if (!this.isDataEmpty()) {
                            this.zone.runOutsideAngular(() => {
                                this.chartData(arrayResult);
                                this.addSerie(series);
                            });
                            setTimeout(() => {
                                this.loadingSource.next(false);
                            }, 3000);
                        }
                        else {
                            this.message = 'No hay datos disponibles en el rango de fechas actual.';
                            this.messageSource.next(this.message);
                            this.loadingSource.next(false);
                        }

                        return;
                    }
                }, (error: HttpErrorResponse) => {
                    this.connectionSource.next(false);
                    this.generateChart(series, initialDate, finalDate);
                });
        }
    }

    chartData(dateProvider) {
        // am4core.useTheme(am4themes_animated);
        // am4core.useTheme(am4themes_dark);
        this.chart = am4core.create('chartdiv', am4charts.XYChart);
        this.chart.hiddenState.properties.opacity = 0;
        this.chart.invalidateData();
        // this.chart.preloader.disabled = true;
        this.chart.nonScalingStroke = true;
        this.chart.language.locale = am4lang_es_ES;
        this.chart.data = dateProvider;
        this.chart.dateFormatter.dateFormat = 'yyyy-MM-dd';
        this.chart.responsive;

        const dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.periodChangeDateFormats.setKey('hour', '[bold]HH:mm a');
        dateAxis.renderer.grid.template.location = 0;
        dateAxis.renderer.minGridDistance = 50;
        dateAxis.tooltipDateFormat = 'HH:mm, d MMMM';
        dateAxis.dataFields.date = 'date';
        dateAxis.baseInterval = {
            'timeUnit': 'second',
            'count': 1
        };

        const valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.grid.template.strokeWidth = 1;
        valueAxis.renderer.minWidth = 60;
        valueAxis.renderer.grid.template.strokeDasharray = '2,3';
        valueAxis.renderer.minWidth = 60;
        valueAxis.renderer.grid.template.disabled = true;
        valueAxis.renderer.minGridDistance = 50;

        // poner rangos horizontales en el gráfico
        /*let range = valueAxis.axisRanges.create();
        range.value = 90.4;
        range.grid.stroke = am4core.color('#396478');
        range.grid.strokeWidth = 1;
        range.grid.strokeOpacity = 1;
        range.grid.strokeDasharray = '3,3';
        range.label.inside = true;
        range.label.text = 'Average';
        range.label.fill = range.grid.stroke;
        range.label.verticalCenter = 'bottom';*/

        this.chart.events.on('datavalidated', function () {
            dateAxis.zoom({ start: 0.8, end: 1 });
        });

    }

    addSerie(serie: ChartParams[]) {
        this.chart.invalidateData();
        for (let index = 0; index < serie.length; index++) {

            if (index >= serie.length) {
                return;
            }
            const element = serie[index].measuringName;
            if (element === 'CO2') {
                const series = this.chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = 'CO2';
                series.dataFields.dateX = 'date';
                series.name = 'CO2';
                series.fill = am4core.color(this.colorContaminant.CO2);
                series.stroke = am4core.color(this.colorContaminant.CO2);
                series.strokeWidth = 3;
                series.tooltipText = 'CO2: [bold]{valueY}[/]';
                series.tensionX = 0.8;
                //let shadow = series.filters.push(new am4core.DropShadowFilter());

            }

            if (element === 'CH4') {
                const series2 = this.chart.series.push(new am4charts.LineSeries());
                series2.dataFields.valueY = 'CH4';
                series2.dataFields.dateX = 'date';
                series2.name = 'CH4';
                series2.fill = am4core.color(this.colorContaminant.CH4);
                series2.stroke = am4core.color(this.colorContaminant.CH4);
                series2.strokeWidth = 3;
                series2.tooltipText = 'CH4: [bold]{valueY}[/]';
                series2.tensionX = 0.8;
                //let shadow = series2.filters.push(new am4core.DropShadowFilter());
            }

            if (element === 'O2') {
                const series3 = this.chart.series.push(new am4charts.LineSeries());
                series3.dataFields.valueY = 'O2';
                series3.dataFields.dateX = 'date';
                series3.name = 'O2';
                series3.fill = am4core.color(this.colorContaminant.O2);
                series3.stroke = am4core.color(this.colorContaminant.O2);
                series3.strokeWidth = 3;
                series3.tooltipText = 'O2: [bold]{valueY}[/]';
                series3.tensionX = 0.8;
                //let shadow = series3.filters.push(new am4core.DropShadowFilter());
            }

            if (element === 'H2S') {
                const series4 = this.chart.series.push(new am4charts.LineSeries());
                series4.dataFields.valueY = 'H2S';
                series4.dataFields.dateX = 'date';
                series4.name = 'H2S';
                series4.fill = am4core.color(this.colorContaminant.H2S);
                series4.stroke = am4core.color(this.colorContaminant.H2S);
                series4.strokeWidth = 3;
                series4.tooltipText = 'H2S: [bold]{valueY}[/]';
                series4.tensionX = 0.8;
                //let shadow = series4.filters.push(new am4core.DropShadowFilter());
            }

        }

        this.chart.legend = new am4charts.Legend();
        // Make a panning cursor
        this.chart.cursor = new am4charts.XYCursor();
        // Create vertical scrollbar and place it before the value axis
        this.chart.scrollbarY = new am4core.Scrollbar();
        this.chart.scrollbarY.parent = this.chart.leftAxesContainer;
        this.chart.scrollbarY.toBack();
        // Add Scrollbar    
        this.chart.scrollbarX = new am4core.Scrollbar();
    }

    removeSeriesFromChart(name) {
        const array = this.namesSource.value;
        this.zone.runOutsideAngular(() => {
            this.chart.series.removeIndex(array.indexOf(name)).dispose();
            for (let i = 0; i < this.chart.data.length; i++) {
                delete this.chart.data[i][name];
            }
        });
        this.names = array.filter(a => a !== name);
        this.namesSource.next(this.names);
        if (this.namesSource.value.length === 0) {
            this.chartValueProvider = [];
            this.isDataEmpty();
        }
    }

    downloadFile(focusDescription, initDate, finalDate) {
        if (this.chart && this.chart.data.length > 0 && focusDescription !== undefined) {
            initDate = moment(initDate).format('YYYY-MM-DD');
            finalDate = moment(finalDate).format('YYYY-MM-DD');
            let csvData = this.chart.data;
            let filter;
            const empty = {};
            for (let i = 0; i < csvData.length; i++) {
                csvData[i].date = empty['date'] = moment(csvData[i].date).format('DD-MM-YYYY HH:mm');
            }

            const options = {
                fieldSeparator: ',',
                quoteStrings: '',
                decimalseparator: '.',
                showLabels: true,
                showTitle: true,
                title: focusDescription,
                useBom: true,
                headers: ['Timestamp', this.namesSource.value]
            };
            for (let i = 0; i < this.namesSource.value.length; i++) {
                filter = csvData.filter(x => {
                    return x[this.namesSource.value[i]] !== null || undefined || 0;
                })
            }
            const csvFile = new Angular5Csv(filter, this.namesSource.value + '_' + initDate + '_' + finalDate, options);
        } else {
            this.openSnackBar('No hay datos para descargar el archivo', 'Ok');
        }
    }

    saveSerieConfig(item: ChartParams[]) {
        const headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json');
        for (const series of item) {
            this.http.post<Serie>('api/serie', series, { headers: headers })
                .subscribe(() => {
                    this.openSnackBar('Series guardadas correctamente', 'Ok');
                });
        }

    }

    deleteSerieConfig(item: ChartParams[]) {
        for (const series of item) {
            this.http.delete<Serie>('api/serie/' + series.id)
                .subscribe(() => {
                    this.openSnackBar('Series eliminadas correctamente', 'Ok');
                });
        }
    }

    isDataEmpty(): boolean {
        for (const key in this.chartValueProvider) {
            if (this.chartValueProvider[key] !== null) {
                return false
            }
        }
        this.chartValueProvider = [];
        return true
    }


    addNames(names) {
        const currentValue = this.namesSource.value;
        const updatedValue = [...currentValue, names];
        this.namesSource.next(updatedValue);
    }

    updateNames(item) {
        this.namesSource.next(item)
    }

    getSeries(): Observable<Serie | null> {
        return this.http.get<Serie>('api/serie');
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            panelClass: ['custom-class'],
            duration: 10000,
        });
    }

    //
}

export interface IColors {
    blue?: string;
    red?: string;
    green?: string;
    orange?: string;
}

export interface IcolorContaminant {
    CO2: string;
    CH4: string;
    H2S: string;
    O2: string;
}