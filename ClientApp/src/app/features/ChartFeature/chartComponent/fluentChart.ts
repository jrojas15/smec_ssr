/* tslint:disable */
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4lang_es_ES from '@amcharts/amcharts4/lang/es_ES';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import * as moment from 'moment';


export class FluentChart implements ChartFunctions {
    private chart: am4charts.XYChart;

    dispose() {
        if (this.chart) {
            console.log('dispose')
            this.chart.dispose();
        }
    }

    addChart(data: any[]) {
        console.log('addChart');
        // console.log('data', data);
        if (this.chart) {
            this.chart.dispose();
        }
        this.chart = am4core.create('chartdiv', am4charts.XYChart);
        this.chart.hiddenState.properties.opacity = 0;
        this.chart.invalidateData();
        this.chart.preloader.disabled = true;
        this.chart.language.locale = am4lang_es_ES;
        this.chart.data = data;
        this.chart.dateFormatter.dateFormat = "yyyy-MM-dd";
        const dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.grid.template.location = 0;
        dateAxis.tooltipDateFormat = 'HH:mm, d MMMM';
        dateAxis.dataFields.date = 'date';
        dateAxis.baseInterval = {
            'timeUnit': 'second',
            'count': 1
        };

        const valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.grid.template.strokeWidth = 1;
        valueAxis.renderer.minWidth = 60;
        valueAxis.renderer.grid.template.strokeDasharray = "2,3";
        valueAxis.renderer.minWidth = 60;
    }

    addSerie(serie: ChartParams[]) {
        console.log('addSerie');
        this.chart.invalidateData();
        for (let index = 0; index < serie.length; index++) {
            const element = serie[index].measuringName;
            if (element === 'CO2') {
                const series = this.chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = 'CO2';
                series.dataFields.dateX = 'date';
                series.name = 'CO2';
                series.fill = am4core.color("#ff0000");
                series.stroke = am4core.color("#ff0000");
                series.strokeWidth = 2;
                series.tooltipText = 'CO2: [bold]{valueY}[/]';
                //series.connect = false;
            }

            if (element === 'CH4') {
                const series2 = this.chart.series.push(new am4charts.LineSeries());
                series2.dataFields.valueY = 'CH4';
                series2.dataFields.dateX = 'date';
                series2.name = 'CH4';
                series2.fill = am4core.color("#0040ff");
                series2.stroke = am4core.color("#0040ff");
                series2.strokeWidth = 2;
                series2.tooltipText = 'CH4: [bold]{valueY}[/]';
                //series2.connect = false;
            }

            if (element === 'O2') {
                const series3 = this.chart.series.push(new am4charts.LineSeries());
                series3.dataFields.valueY = 'O2';
                series3.dataFields.dateX = 'date';
                series3.name = 'O2';
                series3.fill = am4core.color("#ff8000");
                series3.stroke = am4core.color("#ff8000");
                series3.strokeWidth = 2;
                series3.tooltipText = 'O2: [bold]{valueY}[/]';
                //series3.connect = false;
            }

            if (element === 'H2S') {
                const series4 = this.chart.series.push(new am4charts.LineSeries());
                series4.dataFields.valueY = 'H2S';
                series4.dataFields.dateX = 'date';
                series4.name = 'H2S';
                series4.fill = am4core.color("#33cc33");
                series4.stroke = am4core.color("#33cc33");
                series4.strokeWidth = 2;
                series4.tooltipText = 'H2S: [bold]{valueY}[/]';
                //series4.connect = false;
            }

        }

        this.chart.legend = new am4charts.Legend();
        this.chart.cursor = new am4charts.XYCursor();
        this.chart.scrollbarX = new am4core.Scrollbar();
        console.log('data serie', this.chart.data);
    }

    removeFromSVG(names: string[], name: string) {
        this.chart.series.removeIndex(names.indexOf(name)).dispose();
        for (let i = 0; i < this.chart.data.length; i++) {
            delete this.chart.data[i][name];
        }
    }


    dataCSVFormat(headers: string[], description: string, initDate: any, endDate: any) {
        initDate = moment(initDate).format('YYYY-MM-DD');
        endDate = moment(endDate).format('YYYY-MM-DD');
        let csvData = this.chart.data;
        let filter;
        const empty = {};
        for (let i = 0; i < csvData.length; i++) {
            csvData[i].date = empty['date'] = moment(csvData[i].date).format('DD-MM-YYYY HH:mm');
        }

        const options = {
            fieldSeparator: ',',
            quoteStrings: '',
            decimalseparator: '.',
            showLabels: true,
            showTitle: true,
            title: description,
            useBom: true,
            headers: ['Timestamp', headers]
        };

        for (let i = 0; i < headers.length; i++) {
            filter = csvData.filter(x => {
                return x[headers[i]] !== null || undefined || 0;
            })
        }
        const csvFile = new Angular5Csv(filter, headers + '_' + initDate + '_' + endDate, options);
    }
}

export interface ChartFunctions {
    addChart(data: any);
    addSerie(serie: ChartParams[]);
    removeFromSVG(names: string[], name: string);
    dataCSVFormat(headers: string[], description: string, initDate: any, endDate: any);
    dispose();
}

export interface ChartParams {
    id?: number;
    focusId: number;
    sensorId: number;
    description: string;
    measuringName: string;
}
