import { Component, OnInit, AfterViewInit, OnDestroy, NgZone, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FocusService } from '@services/api.focus.service';
import { ChartService } from './chart.service';
// import { FormControl } from '@angular/forms';
import { Focus } from '@models/models';
import { MatMenuTrigger } from '@angular/material';
import { ChartParams } from './chart.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit, OnDestroy {
  title = '¡Atención!';
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  private ngUnsubscribe = new Subject();
  max_date = new Date();
  date = new Date();
  yesterday = new Date(this.date.getTime());
  //  cambiar para prod initialDate;
  initialDate = new Date('November 25, 2018 00:00:00');
  finalDate = new Date('November 27, 2018 00:00:00');
  measuringName: string;
  focusDescription: string;
  temporaryfocusDesc: string;
  focusList: Focus[] = [];
  series: Focus[] = [];
  seriesList: Focus[] = [];
  serieConfigParams: ChartParams[] = [];
  removedConfigfilter: ChartParams[] = [];
  namelist = [];
  savedNameList = [];
  sensors = [];
  step = 0;
  focusId: number;
  sensorId: number;
  serieId: number;
  disableButton = false;
  isLoading = true;
  message: string;
  IsConnected = true;
  items = [];

  constructor(
    private _focusService: FocusService,
    private zone: NgZone,
    public chartService: ChartService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.loaderFunction();
    this.getMeasuringNames();
    // this.yesterday.setDate(this.date.getDate() - 1);
    // this.initialDate = new Date(this.yesterday);
    this.getMeasuringPoint(true);
    this.step--;
  }

  loadChart() {
    if (this.removedConfigfilter.length > 0) {
      this.serieConfigParams = this.removedConfigfilter;
    }
    if (this.serieConfigParams.length > 0 && this.compareDates) {
      this.chartService.generateChart(this.serieConfigParams, this.initialDate, this.finalDate);
      this.removedConfigfilter = [];
    } else {
      this.removedConfigfilter = [];
      this.message = 'No hay series en el foco actual.';
    }
  }

  getMeasuringPoint(bool: boolean) {
    this._focusService.focus_GetAll()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => {
        this.IsConnected = true;
        this.seriesList = res.filter(s => s.series.length !== 0);
        if (typeof res[0] !== undefined && bool) {
          this.focusList = res;
          this.series = res.filter(serie =>
            serie.inService === 1,
          );
          this.serieConfigParams = <any>this.series[0].series;
          this.focusDescription = this.series[0].description;
          if (this.serieConfigParams.length > 0) {
            this.chartService.generateChart(this.serieConfigParams, this.initialDate, this.finalDate);
          } else {
            this.message = 'No hay series en el foco actual.';
          }
        }
      }, (error: HttpErrorResponse) => {
        this.IsConnected = false;
        this.isLoading = false;
        this.getMeasuringPoint(true);
      });
  }

  getMeasuringNames() {
    this.chartService.currentNames.subscribe((res: string[]) => {
      this.namelist = res;
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      // height: '200px',
      width: '250px',
      // data:
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteConfig();
      }
    });
  }

  compareDates() {
    if (this.initialDate < this.finalDate) {
      return true;
    }
    const message = 'La fecha inicial tiene que ser menor que la fecha final.';
    this.chartService.openSnackBar(message, 'Ok');
    return false;
  }

  loaderFunction() {
    this.chartService.isLoading
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => this.isLoading = res);

    this.chartService.curretMessage
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => this.message = res);

    this.chartService.isConnected
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => {
        this.IsConnected = res;
      });
  }

  selectedFocus(focus) {
    this.sensors = focus.analyzers[0].sensors;
    this.temporaryfocusDesc = focus.description;
    this.focusId = focus.focusId;
  }

  chart() {
    this.chartService.updateNames([]);
    this.serieConfigParams = [];
    const item: Iitem = {
      focusId: this.focusId,
      sensorId: this.sensorId,
      description: this.temporaryfocusDesc,
      measuringName: this.measuringName
    };
    this.serieConfigParams.push(item);
    setTimeout(() => {
      this.chartService.addNames(this.measuringName);
      this.loadChart();
      this.focusDescription = this.temporaryfocusDesc;
    }, 1000);
  }

  addSerie() {
    const item: Iitem = {
      focusId: this.focusId,
      sensorId: this.sensorId,
      description: this.focusDescription,
      measuringName: this.measuringName
    };
    const sensorFilter = this.serieConfigParams.filter(x => x.sensorId === item.sensorId);
    const focusFilter = this.serieConfigParams.filter(x => x.focusId === item.focusId);
    if (sensorFilter.length === 0 && focusFilter.length > 0) {
      this.serieConfigParams.push(item);
      setTimeout(() => {
        this.chartService.addNames(this.measuringName);
        this.loadChart();
      }, 1000);
    } else {
      const message = item.measuringName + ' ' + 'ya existe';
      this.chartService.openSnackBar(message, 'Ok');
    }
  }

  saveConfig() {
    const focusFilter = this.seriesList.some(x => this.focusId === x.focusId);
    if (!focusFilter) {
      this.chartService.saveSerieConfig(this.serieConfigParams);
      setTimeout(() => {
        this.getMeasuringPoint(false);
      }, 2000);
    } else {
      this.chartService.updateNames([]);
      const message = this.focusDescription + ' ' + 'ya existe';
      this.chartService.openSnackBar(message, 'Ok');
    }
  }

  getSerieSelected() {
    this.serieConfigParams = [];
    this.serieConfigParams = this.items;
    this.chartService.updateNames([]);
    this.focusDescription = this.items[0].description;
    setTimeout(() => {
      this.loadChart();
    }, 1000);
  }


  openMyMenu(item) {
    this.items = [];
    this.items = item;
    // this.trigger.openMenu();
  }

  closeMyMenu() {
    this.trigger.closeMenu();
  }

  deleteConfig() {
    this.serieConfigParams = this.items;
    this.chartService.deleteSerieConfig(this.serieConfigParams);
    setTimeout(() => {
      this.getMeasuringPoint(false);
    }, 2000);
  }

  removeSeriesChart(name) {
    const filter = this.serieConfigParams.filter(x => x.measuringName !== name);
    this.removedConfigfilter = filter;
    this.chartService.removeSeriesFromChart(name);
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  selectedSensor(sensor) {
    this.measuringName = sensor.measuringComponent.name;
    this.sensorId = sensor.sensorId;
    this.disableButton = true;
  }

  downloadDataToCSV() {
    const description = this.focusDescription || this.temporaryfocusDesc;
    this.chartService.downloadFile(description, this.initialDate, this.finalDate);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this.zone.runOutsideAngular(() => {
      if (this.chartService.chart) {
        this.chartService.chart.dispose();
      }
    });
  }
}


export interface Iitem {
  focusId: number;
  sensorId: number;
  measuringName: string;
  description: string;
}



