import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeatureRootComponentComponent } from './feature-root-component/feature-root-component.component';
import { RoleGuardService as RoleGuard } from '@services/role-guard.service';
const appRoutes: Routes = [
    {
        path: '',
        component: FeatureRootComponentComponent,
        children: [
            {
                path: '',
                redirectTo: 'visualizacion',
                pathMatch: 'full'
            },
            {
                path: 'visualizacion',
                loadChildren: './displayFeature/fetch.module#FetchModule',
                // canActivate: [RoleGuard],
                /*data: {
                    allowedRoles: ['ADMIN', 'SU', 'WORKER', 'VIEW_ONLY']
                }*/
            },
            {
                path: 'graficoHistorico',
                loadChildren: './ChartFeature/chartComponent/chart.module#ChartModule',
                // canActivate: [RoleGuard],
                // canLoad: [RoleGuard],
                /*data: {
                    allowedRoles: ['ADMIN', 'SU', 'WORKER', 'VIEW_ONLY']
                }*/
            }
            /* {
                 path: 'download',
                 loadChildren: './download-feature/download-feature.module#DownloadModule'
             },
             {
                 path: 'upload',
                 loadChildren: './fileFeature/file.module#FileModule'
             },
             {
                 path: 'focus',
                 loadChildren: './focusFeature/focus.module#FocusModule'
             },
             {
                 path: 'calibration',
                 loadChildren: './calibationFunctionsFeature/calibration-function.module#CalibrationFunctionModule'
             },
             {
                 path: 'periferic',
                 loadChildren: './perifericFeature/periferic.module#PerifericModule'
             }*/
        ]
    },

];
@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule]
})
export class FeaturesRoutingModule { }
