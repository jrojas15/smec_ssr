import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@services/auth-guard.service';
import { LoginViewComponent } from './shared/LoginSharedModule/login-view.component';


const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginViewComponent,
  },
  {
    path: 'features',
    loadChildren: './features/features.module#FeaturesModule',
    canActivate: [AuthGuard],
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'login',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
