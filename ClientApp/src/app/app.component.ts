import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router, Event, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
// Components
// Services
import { AuthService } from '@services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  routeData;
  url;
  routerEvent;
  isExpanded = true;
  opened: boolean;
  username: string;
  showLoadingIndicator = true;

  baseUrl: string;

  constructor(
    public authService: AuthService,
    public dialog: MatDialog,
    private router: Router,
  ) {

    this.router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.showLoadingIndicator = true;
      }

      if (routerEvent instanceof NavigationEnd || routerEvent instanceof NavigationCancel || routerEvent instanceof NavigationError) {
        this.showLoadingIndicator = false;
      }
    });
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['features']);
    } else {
      this.router.navigate(['login']);
    }
  }

  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }


}
