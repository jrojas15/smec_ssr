// https://itnext.io/cache-components-with-angular-routereusestrategy-3e4c8b174d5f
// https://github.com/angular/angular/issues/6634
import { RouteReuseStrategy, ActivatedRouteSnapshot, DetachedRouteHandle } from '@angular/router';

function routeToUrl(route: ActivatedRouteSnapshot): string {
    if (route.url) {
        if (route.url.length) {
            return route.url.join('/');
        } else {
            if (typeof route.component === 'function') {
                return `[${route.component.name}]`;
            } else if (typeof route.component === 'string') {
                return `[${route.component}]`;
            } else {
                return `[null]`;
            }
        }
    } else {
        return '(null)';
    }
}

function calcKey(route: ActivatedRouteSnapshot) {
    // const next = route;
    const url = route.pathFromRoot.map(it => routeToUrl(it)).join('/') + '*';
    /* while (next.firstChild) {
         next = next.firstChild;
         url += '/' + routeToUrl(next);
     }*/
    return url;
}
export class CacheRouteReuseStrategy implements RouteReuseStrategy {
    storedRouteHandles = new Map<string, DetachedRouteHandle>();
    // Method called everytime navigate between routes.
    // If it returns TRUE the routing will not happen (which means that routing has not changed).
    // If it returns FALSE then the routing happens and the rest of the methods are called
    shouldReuseRoute(before: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
        const _before = calcKey(before);
        const _curr = calcKey(curr);
        return _before === _curr;
    }

    // This method is called for the route just opened when we land on the component of this route.
    // Once component is loaded this method is called. If this method returns TRUE then retrieve method will be called,
    // otherwise the component will be created from scratch
    shouldAttach(route: ActivatedRouteSnapshot): boolean {
        const key = calcKey(route);
        return !!route.routeConfig && !!this.storedRouteHandles[key];
    }

    // This method is called if shouldAttach returns TRUE, provides as parameter the current route (we just land),
    // and returns a stored RouteHandle. If returns null has no effects. We can use this method to get any stored RouteHandle manually
    retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle | null {
        if (!route.routeConfig) {
            return null;
        }
        const key = calcKey(route);
        return this.storedRouteHandles[calcKey(route)];
        // console.log('ResuseStrategy: retrieve =>', key, route);
        // return this.storedRouteHandles.get(this.getPath(route)) as DetachedRouteHandle;
    }

    // It is invoked when we leave the current route. If returns TRUE then the store method will be invoked
    shouldDetach(route: ActivatedRouteSnapshot): boolean {
        // choose  components to detach
        return route.data && (route.data as any).shouldDetach;
    }

    // This method is invoked only if the shouldDetach returns true. We can manage here how to store the RouteHandle.
    // What we store here will be used in the retrieve method. It provides the route we are leaving and the RouteHandle
    store(route: ActivatedRouteSnapshot, detachedTree: DetachedRouteHandle): void {
        const key = calcKey(route);
        this.storedRouteHandles[key] = detachedTree;
        // console.log('store: storedRouteHandles', this.storedRouteHandles);
    }

}

