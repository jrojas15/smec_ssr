export interface ICurrentData {
    sensorId: number;
    name: string;
    initialDate: any;
    finalDate: any;
}

export class CurrentData implements ICurrentData {
    sensorId: number | undefined;
    name: string | undefined;
    initialDate: any | undefined;
    finalDate: any | undefined;
}
