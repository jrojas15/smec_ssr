export interface IGraphs {
    type?: string;
    lineColor?: string;
    bullet?: string;
    bulletSize?: number;
    hideBulletCount?: number;
    id?: string;
    fillAlphas?: number;
    valueField?: string;
    balloonText?: string;
    title?: string;
    lineThickness?: number;
}

export class Graphs implements IGraphs {
    type?: string;
    lineColor?: string;
    bullet?: string;
    bulletSize?: number;
    hideBulletCount?: number;
    id?: string;
    fillAlphas?: number;
    valueField?: string;
    balloonText?: string;
    title?: string;
    lineThickness?: number;

}

