/* tslint:disable */
import * as moment from 'moment';

export class Focus implements IFocus {
    focusId!: number;
    name?: string | undefined;
    description?: string | undefined;
    inService?: number | undefined;
    analyzers?: Analyzer[] | undefined;
    series?: Serie[] | undefined;

    constructor(data?: IFocus) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.focusId = data["focusId"];
            this.name = data["name"];
            this.description = data["description"];
            this.inService = data["inService"];
            if (data["analyzers"] && data["analyzers"].constructor === Array) {
                this.analyzers = [];
                for (let item of data["analyzers"])
                    this.analyzers.push(Analyzer.fromJS(item));
            }

            if (data["series"] && data["series"].constructor === Array) {
                this.series = [];
                for (let item of data["series"])
                    this.series.push(Serie.fromJS(item));
            }
        }
    }

    static fromJS(data: any): Focus {
        data = typeof data === 'object' ? data : {};
        let result = new Focus();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["focusId"] = this.focusId;
        data["name"] = this.name;
        data["description"] = this.description;
        data["inService"]= this.inService;
        if (this.analyzers && this.analyzers.constructor === Array) {
            data["analyzers"] = [];
            for (let item of this.analyzers)
                data["analyzers"].push(item.toJSON());
        }
        if (this.series && this.series.constructor === Array) {
            data["series"] = [];
            for (let item of this.series)
                data["series"].push(item.toJSON());
        }
        return data; 
    }
}

export interface IFocus {
    focusId: number;
    name?: string | undefined;
    description?: string | undefined;
    inService?: number | undefined;
    analyzers?: Analyzer[] | undefined;
    series?: Serie[] | undefined;
    
}

export class Analyzer implements IAnalyzer {
    analyzerId!: number;
    focusId!: number;
    manufacturer?: string | undefined;
    model?: string | undefined;
    serialNumber?: string | undefined;
    sensors?: Sensor[] | undefined;
    focus?: Focus | undefined;
    IsSelected?: boolean | undefined;

    constructor(data?: IAnalyzer) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.analyzerId = data["analyzerId"];
            this.focusId = data["focusId"];
            this.manufacturer = data["manufacturer"];
            this.model = data["model"];
            this.serialNumber = data["serialNumber"];
            if (data["sensors"] && data["sensors"].constructor === Array) {
                this.sensors = [];
                for (let item of data["sensors"])
                    this.sensors.push(Sensor.fromJS(item));
            }
            this.focus = data["focus"] ? Focus.fromJS(data["focus"]) : <any>undefined;
        }
    }

    static fromJS(data: any): Analyzer {
        data = typeof data === 'object' ? data : {};
        let result = new Analyzer();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["analyzerId"] = this.analyzerId;
        data["focusId"] = this.focusId;
        data["manufacturer"] = this.manufacturer;
        data["model"] = this.model;
        data["serialNumber"] = this.serialNumber;
        if (this.sensors && this.sensors.constructor === Array) {
            data["sensors"] = [];
            for (let item of this.sensors)
                data["sensors"].push(item.toJSON());
        }
        data["focus"] = this.focus ? this.focus.toJSON() : <any>undefined;
        return data; 
    }
}

export interface IAnalyzer {
    analyzerId: number;
    focusId: number;
    manufacturer?: string | undefined;
    model?: string | undefined;
    serialNumber?: string | undefined;
    sensors?: Sensor[] | undefined;
    focus?: Focus | undefined;
    IsSelected?: boolean | undefined;
}


export class CalibrationFunction implements ICalibrationFunction {
    calibrationFunctionId!: number;
    sensorId!: number;
    date?: moment.Moment | undefined;
    slope!: number;
    intercept!: number;
    sensor?: Sensor | undefined;

    constructor(data?: ICalibrationFunction) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.calibrationFunctionId = data["calibrationFunctionId"];
            this.sensorId = data["sensorId"];
            this.date = data["date"] ? moment(data["date"].toString()) : <any>undefined;
            this.slope = data["slope"];
            this.intercept = data["intercept"];
            this.sensor = data["sensor"] ? Sensor.fromJS(data["sensor"]) : <any>undefined;
        }
    }

    static fromJS(data: any): CalibrationFunction {
        data = typeof data === 'object' ? data : {};
        let result = new CalibrationFunction();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["calibrationFunctionId"] = this.calibrationFunctionId;
        data["sensorId"] = this.sensorId;
        data["date"] = this.date ? this.date.format('YYYY-MM-DD') : <any>undefined;
        data["slope"] = this.slope;
        data["intercept"] = this.intercept;
        data["sensor"] = this.sensor ? this.sensor.toJSON() : <any>undefined;
        return data; 
    }
}

export interface ICalibrationFunction {
    calibrationFunctionId: number;
    sensorId: number;
    date?: moment.Moment | undefined;
    slope: number;
    intercept: number;
    sensor?: Sensor | undefined;
}

export class Sensor implements ISensor {
    sensorId!: number;
    analyzerId!: number;
    measuringComponentId!: number;
    unitId!: number;
    calibrationFunctions?: CalibrationFunction[] | undefined;
    analyzer?: Analyzer | undefined;
    measuringComponent?: MeasuringComponent | undefined;
    unit?: Unit | undefined;
    periferic?: Periferic | undefined;
    isSelected?: boolean;

    constructor(data?: ISensor) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.sensorId = data["sensorId"];
            this.analyzerId = data["analyzerId"];
            this.measuringComponentId = data["measuringComponentId"];
            this.unitId = data["unitId"];
            if (data["calibrationFunctions"] && data["calibrationFunctions"].constructor === Array) {
                this.calibrationFunctions = [];
                for (let item of data["calibrationFunctions"])
                    this.calibrationFunctions.push(CalibrationFunction.fromJS(item));
            }
            this.analyzer = data["analyzer"] ? Analyzer.fromJS(data["analyzer"]) : <any>undefined;
            this.measuringComponent = data["measuringComponent"] ? MeasuringComponent.fromJS(data["measuringComponent"]) : <any>undefined;
            this.unit = data["unit"] ? Unit.fromJS(data["unit"]) : <any>undefined;
            this.periferic = data["periferic"] ? Periferic.fromJS(data["periferic"]) : <any>undefined;
        }
    }

    static fromJS(data: any): Sensor {
        data = typeof data === 'object' ? data : {};
        let result = new Sensor();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["sensorId"] = this.sensorId;
        data["analyzerId"] = this.analyzerId;
        data["measuringComponentId"] = this.measuringComponentId;
        data["unitId"] = this.unitId;
        if (this.calibrationFunctions && this.calibrationFunctions.constructor === Array) {
            data["calibrationFunctions"] = [];
            for (let item of this.calibrationFunctions)
                data["calibrationFunctions"].push(item.toJSON());
        }
        data["analyzer"] = this.analyzer ? this.analyzer.toJSON() : <any>undefined;
        data["measuringComponent"] = this.measuringComponent ? this.measuringComponent.toJSON() : <any>undefined;
        data["unit"] = this.unit ? this.unit.toJSON() : <any>undefined;
        data["periferic"] = this.periferic ? this.periferic.toJSON() : <any>undefined;
        return data; 
    }
}

export interface ISensor {
    sensorId: number;
    analyzerId: number;
    measuringComponentId: number;
    unitId: number;
    calibrationFunctions?: CalibrationFunction[] | undefined;
    analyzer?: Analyzer | undefined;
    measuringComponent?: MeasuringComponent | undefined;
    unit?: Unit | undefined;
    periferic?: Periferic | undefined;
    isSelected?: boolean;
}

export class Periferic implements IPeriferic {
    perifericId!: number;
    sensorId!: number;
    formula?: string | undefined;
    sensor?: Sensor | undefined;
    isEditable?: boolean;

    constructor(data?: IPeriferic) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.perifericId = data["perifericId"];
            this.sensorId = data["sensorId"];
            this.formula = data["formula"];
            this.sensor = data["sensor"] ? Sensor.fromJS(data["sensor"]) : <any>undefined;
        }
    }

    static fromJS(data: any): Periferic {
        data = typeof data === 'object' ? data : {};
        let result = new Periferic();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["perifericId"] = this.perifericId;
        data["sensorId"] = this.sensorId;
        data["formula"] = this.formula;
        data["sensor"] = this.sensor ? this.sensor.toJSON() : <any>undefined;
        return data; 
    }
}

export interface IPeriferic {
    perifericId: number;
    sensorId: number;
    formula?: string | undefined;
    sensor?: Sensor | undefined;
    isEditable?: boolean;
}

export class SensorPeriferic implements ISensorPeriferic {
    sensorPerifericId!: number;
    sensorId!: number;
    perifericId!: number;
    periferic?: Periferic | undefined;

    constructor(data?: ISensorPeriferic) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.sensorPerifericId = data["sensorPerifericId"];
            this.sensorId = data["sensorId"];
            this.perifericId = data["perifericId"];
            this.periferic = data["periferic"] ? Periferic.fromJS(data["periferic"]) : <any>undefined;
        }
    }

    static fromJS(data: any): SensorPeriferic {
        data = typeof data === 'object' ? data : {};
        let result = new SensorPeriferic();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["sensorPerifericId"] = this.sensorPerifericId;
        data["sensorId"] = this.sensorId;
        data["perifericId"] = this.perifericId;
        data["periferic"] = this.periferic ? this.periferic.toJSON() : <any>undefined;
        return data; 
    }
}

export interface ISensorPeriferic {
    sensorPerifericId: number;
    sensorId: number;
    perifericId: number;
    periferic?: Periferic | undefined;
}

export interface ICurrentAnalogData {
    value: number;
    statusCode: number;
    samples: number;
  }
  

export class MeasuringComponent implements IMeasuringComponent {
    measuringComponentId!: number;
    name?: string | undefined;

    constructor(data?: IMeasuringComponent) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.measuringComponentId = data["measuringComponentId"];
            this.name = data["name"];
        }
    }

    static fromJS(data: any): MeasuringComponent {
        data = typeof data === 'object' ? data : {};
        let result = new MeasuringComponent();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["measuringComponentId"] = this.measuringComponentId;
        data["name"] = this.name;
        return data; 
    }
}

export interface IMeasuringComponent {
    measuringComponentId: number;
    name?: string | undefined;
}

export class Unit implements IUnit {
    unitId!: number;
    name?: string | undefined;

    constructor(data?: IUnit) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.unitId = data["unitId"];
            this.name = data["name"];
        }
    }

    static fromJS(data: any): Unit {
        data = typeof data === 'object' ? data : {};
        let result = new Unit();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["unitId"] = this.unitId;
        data["name"] = this.name;
        return data; 
    }
}

export interface IUnit {
    unitId: number;
    name?: string | undefined;
}

export class Serie implements ISerie{
    id!: number;
    focusId?: number | undefined;
    sensorId?: number | undefined;
    description?: string | undefined;
    measuringName?: string | undefined;

    constructor(data?: ISerie) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.focusId = data["focusId"];
            this.sensorId = data["sensorId"];
            this.description = data["description"];
            this.measuringName = data["measuringName"];            
        }
    }

    static fromJS(data: any): Serie {
        data = typeof data === 'object' ? data : {};
        let result = new Serie();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["focusId"] = this.focusId;
        data["sensorId"] = this.sensorId;
        data["description"] = this.description;
        data["measuringName"]= this.measuringName;
        return data; 
    }
}

export interface ISerie {
    id: number;
    focusId?: number | undefined;
    sensorId?: number | undefined;
    description?: string | undefined;
    measuringName?: string | undefined
}

export interface FileResponse {
    data: Blob;
    status: number;
    fileName?: string;
    headers?: { [name: string]: any };
}

export class SwaggerException extends Error {
    message: string;
    status: number; 
    response: string; 
    headers: { [key: string]: any; };
    result: any; 

    constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
        super();

        this.message = message;
        this.status = status;
        this.response = response;
        this.headers = headers;
        this.result = result;
    }

    protected isSwaggerException = true;

    static isSwaggerException(obj: any): obj is SwaggerException {
        return obj.isSwaggerException === true;
    }
}