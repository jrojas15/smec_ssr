/* tslint:disable */

import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, from as _observableFrom, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import * as moment from 'moment';
import { ICurrentAnalogData } from '@models/models';


@Injectable()
export class CurrentAnalogDataService {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(@Inject(HttpClient) http: HttpClient) {
        this.http = http;

    }

    sensor_GetCurrentAnalogData(id: number): Observable<ICurrentAnalogData[] | null> {
        let url_ = "api/Sensor/{id}/CurrentAnalogData";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        url_ = url_.replace(/[?&]$/, "");

        return this.http.get<ICurrentAnalogData[]>(url_)
    }

    sensor_GetHistoricalAnalogData(id: number, start_date: moment.Moment, end_date: moment.Moment): Observable<any[] | null> {
        let url_ = "api/Sensor/{id}/HistoricalAnalogData/{start_date}/{end_date}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        if (start_date === undefined || start_date === null)
            throw new Error("The parameter 'start_date' must be defined.");
        url_ = url_.replace("{start_date}", encodeURIComponent("" + start_date));
        if (end_date === undefined || end_date === null)
            throw new Error("The parameter 'end_date' must be defined.");
        url_ = url_.replace("{end_date}", encodeURIComponent("" + end_date));
        url_ = url_.replace(/[?&]$/, "");

        return this.http.get<any[]>(url_);
    }

    AMChart_GetHistoricalAnalogData(id: number, start_date: moment.Moment, end_date: moment.Moment): Observable<any[] | null> {
        let url_ = "api/AmCharts/{id}/ChartAnalogData/{start_date}/{end_date}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        if (start_date === undefined || start_date === null)
            throw new Error("The parameter 'start_date' must be defined.");
        url_ = url_.replace("{start_date}", encodeURIComponent("" + start_date));
        if (end_date === undefined || end_date === null)
            throw new Error("The parameter 'end_date' must be defined.");
        url_ = url_.replace("{end_date}", encodeURIComponent("" + end_date));
        url_ = url_.replace(/[?&]$/, "");

        return this.http.get<any[]>(url_);
    }


}
