import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {
    private data: any;
    private dataSource = new BehaviorSubject(this.data);
    private analyzerSource = new BehaviorSubject(this.data);
    private sensorSource = new BehaviorSubject(this.data);
    private boolean = new BehaviorSubject(this.data);

    graphConfig: any;
    private graphConfigSubject = new BehaviorSubject(this.graphConfig);

    currentData = this.dataSource.asObservable();
    currentAnalyzer = this.analyzerSource.asObservable();
    currentSensor = this.sensorSource.asObservable();
    currentBoolean = this.boolean.asObservable();
    currentGraphConfig = this.graphConfigSubject.asObservable();


    constructor() { }

    setGraphConfig(data) {
        this.graphConfigSubject.next(data);
    }

    newData(data) {
        this.dataSource.next(data);
    }

    setSensor(data) {
        this.sensorSource.next(data);

    }
    setAnalyzer(data) {
        this.analyzerSource.next(data);
    }

    setBoolean(data) {
        this.boolean.next(data);
    }


}
