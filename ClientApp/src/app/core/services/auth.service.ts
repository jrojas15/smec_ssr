import { throwError as observableThrowError, Observable } from 'rxjs';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { TokenResponse } from './token.response';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthService {
    authKey = 'auth';
    clientId = '';
    baseUrl: string;

    constructor(
        private http: HttpClient,
        @Inject(PLATFORM_ID) private platformId: any,
        private router: Router
    ) {

    }

    login(username: string, password: string): Observable<any> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        // headers.append('Content-Type', 'application/json');

        const url = 'api/token/auth';
        const data = {
            username: username,
            password: password,
            client_id: this.clientId,
            grant_type: 'password',
            scope: 'offline_acces profile email'
            // roles: ['Administrator', 'SuperUser', 'Operator']
        };
        return this.http.post<TokenResponse>(url, data, { headers: headers }).pipe(
            map((res: TokenResponse) => {
                const token = res && res.token;
                // if the token is there, login has been successful
                if (token) {
                    this.setAuth(res);
                    localStorage.setItem('currentUser', username);
                    // successful login
                    // console.log('successful login =>', url, data);
                    return true;
                }
                // failed login
                return observableThrowError('Unauthorized');
            }));
    }

    // logout
    logout(): boolean {
        this.setAuth(null);
        localStorage.removeItem('currentUser');
        this.router.navigate(['login']);
        return true;
    }

    setAuth(auth: TokenResponse | null): boolean {
        if (isPlatformBrowser(this.platformId)) {
            if (auth) {
                localStorage.setItem(
                    this.authKey,
                    JSON.stringify(auth));
            } else {
                localStorage.removeItem(this.authKey);
            }
        }
        return true;
    }
    getAuth(): TokenResponse | null {
        if (isPlatformBrowser(this.platformId)) {
            const i = localStorage.getItem(this.authKey);
            if (i) {
                return JSON.parse(i);
            }
        }
        return null;
    }
    isLoggedIn(): boolean {
        if (isPlatformBrowser(this.platformId)) {
            return localStorage.getItem(this.authKey) != null;
        }
        return false;
    }

}
