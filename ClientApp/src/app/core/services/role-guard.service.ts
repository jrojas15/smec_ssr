import { Injectable } from '@angular/core';
import {
    Router,
    CanActivate,
    ActivatedRouteSnapshot,
    CanLoad,
    Route
} from '@angular/router';
import { AuthService } from './auth.service';


@Injectable()
export class RoleGuardService implements CanActivate, CanLoad {
    role: string;
    expectedRole: string[];
    constructor(public auth: AuthService, public router: Router) { }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        this.expectedRole = route.data.allowedRoles;
        const user = localStorage.getItem('currentUser');
        if (user === 'jonathan') {
            this.role = 'SU';
        }
        if (user === 'smec') {
            this.role = 'WORKER';
        }

        if (user === 'operador') {
            this.role = 'VIEW_ONLY';
        }
        if (
            !this.auth.isLoggedIn() || !this.expectedRole.includes(this.role)
        ) {
            this.router.navigate(['features/' + '']);
            return false;
        }
        return true;

    }

    canLoad(route: Route): boolean {
        if (this.auth.isLoggedIn && this.isAuthorized(['SU', 'WORKER'])) {
            return true;
        }
        return false;
    }

    isAuthorized(expectedRoles: string[]) {
        return expectedRoles.includes(this.role);
    }

}
