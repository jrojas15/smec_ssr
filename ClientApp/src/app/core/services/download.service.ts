/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';

@Injectable()
export class DownloadService {


  constructor(private http: HttpClient) {

  }

  getFiles() {
    return this.http.get('api/values/files');
  }

  downloadFileSelected(file) {
   // window.open('https://localhost:5001/api/values/files/' + file);
    window.open('https://192.168.10.105:4443/api/values/files/' + file);
  }
}
