import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-display-messages',
  templateUrl: './display-messages.component.html',
  styleUrls: ['./display-messages.component.scss']
})
export class DisplayMessagesComponent implements OnInit {
  @Input() title: string;
  @Input() message: string;
  @Input() isDataEmpty?: boolean;
 // @Input() isLoading?: boolean;
  @Input() isConnected: boolean;

  constructor() { }

  ngOnInit() {
  }

}
