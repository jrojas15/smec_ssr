import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayMessagesComponent } from './display-messages.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';


@NgModule({
  imports: [
    CommonModule,
    MatProgressBarModule
  ],
  declarations: [
    DisplayMessagesComponent
  ],
  exports: [
    CommonModule,
    DisplayMessagesComponent,
    MatProgressBarModule,
  ]
})
export class DisplayMessagesModule { }
