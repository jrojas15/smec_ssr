import { Component, OnInit, Inject } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
// Models

// Services
import { FocusService } from '@services/api.focus.service';
import { AnalyzerService } from '@services/api.analyzer.service';
import { CurrentAnalogDataService } from '@services/api.currentAnalogData.service';

@Component({
  selector: 'app-historical-component',
  templateUrl: './historical-component.component.html',
  styleUrls: ['./historical-component.component.css']
})
export class HistoricalComponent implements OnInit {
  csvFile;
  dataJSON;
  dataToCsv;
  focus;
  focoSeleccionado: string;
  focoDescripcionSeleccionado: string;
  measuringSeleccionado: string;
  isSelected = false;
  fechaInicio;
  fechaFinal;
  sensor;
  analyzerId;
  analyzer;
  analyzerSeleccionado;
  dataFormated;
  sensorMeasuring;
  sensorSeleccionado;
  canDownload = false;
  progressBar: boolean;
  formControl;

  constructor(
    private _currentAnalogDataService: CurrentAnalogDataService,
    private _analyzerService: AnalyzerService,
    private _focusService: FocusService,
    public dialogRef: MatDialogRef<HistoricalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

  }

  ngOnInit() {
    this.formControl = new FormControl('', [
      Validators.required
    ]);
    this.getAllFocus();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  getAllFocus() {
    this._focusService.focus_GetAll()
      .subscribe(data => {
        this.dataJSON = data;
      });
  }

  refrescar() {
    this.getAllFocus();
    this.sensorSeleccionado = '';
    this.analyzerSeleccionado = '';
    this.sensorMeasuring = '';
    this.fechaInicio = '';
    this.fechaFinal = '';
  }

  focoIdSeleccionado(id) {
    return forkJoin(
      this._focusService.focus_GetAll(), // data[0]
      this._focusService.focus_GetById(id) // data[1]
    ).subscribe(data => {
      this.focus = data[1];
      this.dataFormated = this.focus.analyzers.map(i => {
        return {
          analyzerId: i.analyzerId,
          manufacturer: i.manufacturer,
        };
      });

    }, (error: HttpErrorResponse) => {
      console.log(error.name + ' ' + error.message);
    });
  }

  analyzerIdSeleccionado(id) {
    this.analyzerId = id;
    this._analyzerService.analyzer_GetById(id)
      .subscribe(data => {
        this.analyzer = data;
        this.sensor = this.analyzer.sensors.map(i => {
          return {
            sensorId: i.sensorId,
            measuringComponent: i.measuringComponent.name
          };
        });
      }, (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getSensorId(op) {
    this.sensorSeleccionado = op;
    console.log('seleccion :', this.sensorSeleccionado);
  }
  // http://localhost:63026/api/sensor/1/historicalanalogdata/2017-11-01/2017-12-25

  historicalData() {
    if (this.sensorSeleccionado.sensorId && this.fechaInicio && this.fechaFinal) {
      const fInicio = moment(this.fechaInicio).format('YYYY-MM-DD');
      const fFinal = moment(this.fechaFinal).format('YYYY-MM-DD');
      console.log('historical Data: ', this.sensorSeleccionado.sensorId, fInicio, fFinal);
      if (fInicio < fFinal) {
     /* this._currentAnalogDataService.sensor_GetHistoricalAnalogData(this.sensorSeleccionado.sensorId, fInicio, fFinal)
        .subscribe(data => {
          console.log('historical Data', data);
          this.dataJSON = data;
          this.dataToCsv = this.dataJSON.map(i => {
            return {
              time: i.timeStamp,
              value: i.value,
              status: i.statusCode,
              samples: i.samples
            };
          });
          this.progressBar = true;
          setTimeout(() => {
            this.progressBar = false;
            this.canDownload = true;
          }, 2000);
        }, (error: HttpErrorResponse) => {
          console.log(error.name + ' ' + error.message);
        });*/
      } else {
      }
    } else {
    }
  }

  generarCSVApi() {
    if (this.focus.description && this.sensorMeasuring.measuringComponent && this.dataToCsv) {
      this.progressBar = true;
      console.log('se peude generar csv');
      const options = {
        fieldSeparator: ',',
        quoteStrings: '',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        title: this.focus.description,
        useBom: true,
        headers: ['Timestamp', this.sensorMeasuring.measuringComponent, 'Status code', 'Samples']
      };
      this.csvFile = new Angular5Csv(this.dataToCsv, 'HistoricalData', options);
      console.log('data to csv', this.dataToCsv);
      setTimeout(() => {
        this.progressBar = false;
        this.dialogRef.close();
      }, 2000);

    } else {
    }
  }

}

