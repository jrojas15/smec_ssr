import { Component, OnInit } from '@angular/core';
import { speedDialFabAnimations } from './speed-dial-fab.animations';
import { MatDialog } from '@angular/material';
import { HistoricalComponent } from '@shared/historicalShared/historical/historical-component.component';

@Component({
  selector: 'app-speed-dial-fab',
  templateUrl: './speed-dial-fab.component.html',
  styleUrls: ['./speed-dial-fab.component.scss'],
  animations: speedDialFabAnimations
})
export class SpeedDialFabComponent implements OnInit {

  fabButtons;
  buttons = [];
  fabTogglerState = 'inactive';

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.fabButtons = [
      {
        icon: 'timeline',
        tooltip: ''
      },
      {
        icon: 'view_headline',
        tooltip: ''
      },
      {
        icon: 'room',
        tooltip: ''
      },
      {
        icon: 'lightbulb_outline',
        tooltip: ''
      },
      {
        icon: 'history',
        tooltip: 'Descargar históricos en csv'
      }
    ];
  }

  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  onToggleFab() {
    this.buttons.length ? this.hideItems() : this.showItems();
  }

  openDialog(fab) {
    if (fab === 'history') {
      const dialogRef = this.dialog.open(HistoricalComponent, {
        height: '400px',
        width: '600px',
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    }
  }

}
