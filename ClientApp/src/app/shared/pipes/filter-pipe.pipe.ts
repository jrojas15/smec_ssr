import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPipe'
})
export class FilterPipePipe implements PipeTransform {

  transform(items: any, args: any): any[] {
    if (!items || !args) {
      return items;
    }
    return items.filter((item: any) => item.periferic != null);
  }

}

