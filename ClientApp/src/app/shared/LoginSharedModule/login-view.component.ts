import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, NavigationEnd } from '@angular/router';
// Services
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
export class LoginViewComponent implements OnInit {

  form: FormGroup;
  hide = true;
  isLogged = true;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authservice: AuthService,
  ) {
    // this.router.routeReuseStrategy.shouldReuseRoute = function () {
    //   return true;
    // };
  }


  ngOnInit() {
    this.form = this.fb.group({
      Username: ['', Validators.required],
      Password: ['', Validators.required]
    });
  }

  get Username() { return this.form.get('Username'); }
  get Password() { return this.form.get('Password'); }

  onSubmit() {

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    const username = this.form.value.Username;
    const password = this.form.value.Password;
    this.authservice.login(username, password)
      .subscribe(res => {

        this.router.navigate(['features']);
      },
        err => {
          // login failed
          this.isLogged = false;
          this.form.setErrors({
            'auth': 'Usuario o contraseña inconrrectos'
          });
        });
  }

  onBack() {
    this.router.navigate(['features']);
  }
  // retrieve a FormControl
  getFormControl(name: string) {
    return this.form.get(name);
  }
  // returns TRUE if the FormControl is valid
  isValid(name: string) {
    const e = this.getFormControl(name);
    return e && e.valid;
  }
  // returns TRUE if the FormControl has been changed
  isChanged(name: string) {
    const e = this.getFormControl(name);
    return e && (e.dirty || e.touched);
  }
  // returns TRUE if the FormControl is invalid after user changes
  hasError(name: string) {
    const e = this.getFormControl(name);
    return e && (e.dirty || e.touched) && !e.valid;
  }

  getPassswordErrorMessage() {
    return this.Password.hasError('required') ? 'Debes escribir una contraseña' :
      this.Password.hasError('Password') ? 'Contraseña no válida' : '';
  }

  getUserNameErrorMessage() {
    return this.Username.hasError('required') ? 'Debes escribir un nombre de usuario' : '';
  }

}
