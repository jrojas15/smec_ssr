import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppMaterialModule } from '../../app-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginViewComponent } from './login-view.component';
import { AuthService } from '@services/auth.service';


@NgModule({
    imports: [
        CommonModule,
        AppMaterialModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        LoginViewComponent
    ],
    providers: [
        AuthService
    ]
})
export class LoginModule { }
