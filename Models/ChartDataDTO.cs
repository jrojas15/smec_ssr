using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    public class ChartDataDTO
    {
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public System.Nullable<double> Value { get; set; }
    }
}
