﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SMECService.Models
{
    public class AnalogDataDTO
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public System.Nullable<double> Value { get; set; }
        public System.Nullable<int> StatusCode { get; set; }
        public System.Nullable<int> Samples { get; set; }

    }
}
