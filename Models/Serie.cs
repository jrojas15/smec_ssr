using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    [Table("Serie")]
    public class Serie
    {
        public int Id { get; set; }
        public int FocusId { get; set; }
        public int SensorId { get; set; }
        public string Description { get; set; }
        public string MeasuringName { get; set; }
        public Focus Focus { get; set; }
        public Sensor Sensor { get; set; }

    }
}
