using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    [Table("SensorPeriferic")]
    public class SensorPeriferic
    {
        public int SensorPerifericId { get; set; }
        public int SensorId { get; set; }
        public int PerifericId { get ; set; }
     
        public  Periferic Periferic { get; set; }
       


    }
}
