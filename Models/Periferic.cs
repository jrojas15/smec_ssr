using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    [Table("Periferic")]
    public class Periferic
    {
        public int PerifericId { get; set; }
        public int SensorId { get; set; }
        public string Formula { get ; set; }   
        public Sensor Sensor { get; set; }
      

    }
}
