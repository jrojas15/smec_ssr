﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

using SMECService.Models;
using SMECService.Data;
using Microsoft.AspNetCore.SignalR;
using SMECService.Hubs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class SensorController : Controller
    {
        private readonly CEMSContext _context;
        private readonly IHubContext<DataHub> hubContext;

        public SensorController(CEMSContext context, IHubContext<DataHub> hub)
        {
            _context = context;
            hubContext = hub;
        }

        [HttpGet]
        public IEnumerable<Sensor> GetAll()
        {
            return _context.Sensors
                .Include(s => s.Analyzer)
                .Include(s => s.MeasuringComponent)
                .Include(s => s.Unit)
                .Include(s => s.CalibrationFunctions)
                .ToList();

        }

        [HttpGet("{id}", Name = "GetSensor")]
        public IActionResult GetById(long id)
        {
            var item = _context.Sensors
                .Include(s => s.Analyzer)
                .Include(s => s.MeasuringComponent)
                .Include(s => s.Unit)
                .Include(s => s.CalibrationFunctions)

                .FirstOrDefault(s => s.SensorId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // periferics 
        [HttpGet("Periferic")]
        public IEnumerable<Sensor> GetSensorPeriferic()
        {
            var query = (from s in _context.Sensors
                         join p in _context.Periferics on s.SensorId equals p.SensorId
                         select s
                   );
            return query
                .Include(s => s.Analyzer)
                     .ThenInclude(f => f.Focus)
                .Include(s => s.MeasuringComponent)
                .Include(s => s.Unit)
                .Include(p => p.Periferic)
                .ToList();
        }

        // periferic by id
        [HttpGet("{id}/Periferic")]
        public IActionResult GetSensorPerifericByÏd(long id)
        {
            var query = (from s in _context.Sensors
                         join p in _context.Periferics on s.SensorId equals p.SensorId
                         select s
                  )
              .Include(s => s.Analyzer)
                  .ThenInclude(f => f.Focus)
              .Include(s => s.MeasuringComponent)
              .Include(s => s.Unit)
              .Include(p => p.Periferic)

              .FirstOrDefault(s => s.SensorId == id);
            if (query == null)
            {
                return NotFound();
            }
            return new ObjectResult(query);
        }

        /* API QUERY SAMPLE 
         * http://localhost:63389/api/Sensor/1/CurrentAnalogData
         * 404 is returned if date format in server locale is wrong
         */
        [HttpGet("{id}/CurrentAnalogData", Name = "GetCurrentAnalogData")]
        public IActionResult GetCurrentAnalogData(long id)
        {
            var item = _context.CurrentAnalogData
                .FirstOrDefault(t => t.Id == id);

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(new AnalogDataDTO()
            {
                TimeStamp = item.TimeStamp,
                Value = item.Value,
                Samples = item.Samples,
                StatusCode = 1
            });
        }

        /* API QUERY SAMPLE 
         * http://localhost:63389/api/Sensor/1/HistoricalAnalogData/10-26-2017/10-27-2017
         * 404 is returned if date format in server locale is wrong
         */
        [HttpGet("{id}/HistoricalAnalogData/{start_date:datetime}/{end_date:datetime}", Name = "GetHistoricalAnalogData")]
        public IActionResult GetHistoricalAnalogData(long id, DateTime start_date, DateTime end_date)
        {
            /* QueryObjects => DTO */

            var items = _context.HistoricalAnalogData
                    .Where(a => a.Id == id && a.TimeStamp >= start_date && a.TimeStamp <= end_date)
                    .Select(p =>
                        new AnalogDataDTO()
                        {
                            TimeStamp = p.TimeStamp,
                            Value = p.Value,
                            StatusCode = 1,
                            Samples = p.Samples
                        });

            if (items == null)
            {
                return NotFound();
            }

            List<AnalogDataDTO> items_wogaps = new List<AnalogDataDTO>();


            bool gap_restart = true;
            AnalogDataDTO lastItem = null;

            foreach (AnalogDataDTO item in items)
            {
                while (start_date < item.TimeStamp)
                {
                    gap_restart = true;

                    if (lastItem != null)
                        items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = lastItem.Value, StatusCode = lastItem.StatusCode });
                    else
                        items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = null, StatusCode = null });
                    start_date = start_date.AddMinutes(1);
                }
                if (gap_restart)
                {
                    gap_restart = false;
                    if (lastItem != null)
                        items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = lastItem.Value, StatusCode = lastItem.StatusCode });
                    else
                        items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = null, StatusCode = null });
                }
                else
                {
                    items_wogaps.Add(item);
                    lastItem = item;
                }
                start_date = start_date.AddMinutes(1);
            }

            while (start_date < end_date)
            {
                items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = null, StatusCode = null });
                start_date = start_date.AddMinutes(1);
            }

            return new ObjectResult(items_wogaps);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Sensor item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _context.Sensors.Add(item);
            _context.SaveChanges();
            await hubContext.Clients.All.SendAsync("add");

            return CreatedAtRoute("GetSensor", new { id = item.SensorId }, item);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var item = _context.Sensors.FirstOrDefault(t => t.SensorId == id);
            if (item == null)
            {
                return NotFound();
            }
            _context.Sensors.Remove(item);
            _context.SaveChanges();
            await hubContext.Clients.All.SendAsync("delete");
            return new NoContentResult();

        }

        //UPDATE : api/sensor/1
        [HttpPatch("{id}")]
        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] Sensor item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.SensorId)
            {
                return BadRequest();
            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
                await hubContext.Clients.All.SendAsync("update");

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        private bool ItemExists(int id)
        {
            return _context.Sensors.Any(c => c.SensorId == id);
        }
    }
}
