using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SMECService.Models;
using SMECService.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;



namespace SMECService.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    public class PerifericController : Controller
    {
        private readonly CEMSContext _context;

        public PerifericController(CEMSContext context)
        {
            _context = context;
        }

        // Get CAlibrationFunctions
        
        [HttpGet]
        public IEnumerable<Periferic> GetAll()
        {
          return _context.Periferics
                
                    .Include(p => p.Sensor)
                         .ThenInclude(m => m.MeasuringComponent)
                     .Include(p => p.Sensor)
                         .ThenInclude(a => a.Analyzer)
                     .Include(p => p.Sensor)
                          .ThenInclude(u => u.Unit)
                     .ToList();
        }

        //Get by sensorId
        [HttpGet("{id}", Name = "GetPeriferic")]
        public IActionResult GetById(long id)
        {
            var item = _context.Periferics
                .Include(p => p.Sensor)
                    .ThenInclude(m => m.MeasuringComponent)
                .FirstOrDefault(p => p.PerifericId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        //POST : api/periferic/create
        [HttpPost("Create")]
        public IActionResult Create([FromBody] Periferic item)
        {
            if (item == null)
            {
                return BadRequest();
            }
            _context.Periferics.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetPeriferic", new { id = item.PerifericId }, item);

        }

        //DELETE : api/periferic/1
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var item = _context.Periferics.FirstOrDefault(t => t.PerifericId == id);
            if (item == null)
            {
                return NotFound();
            }

            _context.Periferics.Remove(item);
            _context.SaveChanges();
            return new NoContentResult();
        }

        //UPDATE : api/calibrationfunction/1
        [HttpPatch("{id}")]
        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] Periferic item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.PerifericId)
            {
                return BadRequest();
            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        private bool ItemExists(int id)
        {
            return _context.Periferics.Any(c => c.PerifericId == id);
        }





    }


}