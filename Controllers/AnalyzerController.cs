﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

using SMECService.Models;
using SMECService.Data;
using Microsoft.AspNetCore.SignalR;
using SMECService.Hubs;



// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SMECService.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    public class AnalyzerController : Controller
    {
        private readonly CEMSContext _context;
        private readonly IHubContext<DataHub> hubContext;

        public AnalyzerController(CEMSContext context, IHubContext<DataHub> hub)
        {
            _context = context;
            hubContext = hub;
        }

        [HttpGet]
        public IEnumerable<Analyzer> GetAll()
        {
            return _context.Analyzers
                .Include(a => a.Focus)
                .Include(a => a.Sensors)
                    .ThenInclude(s => s.MeasuringComponent)
                .Include(a => a.Sensors)
                     .ThenInclude(s => s.Unit)
                .ToList();
        }

        [HttpGet("{id}", Name = "GetAnalyzer")]
        public IActionResult GetById(long id)
        {
            var item = _context.Analyzers
                .Include(a => a.Focus)
                .Include(a => a.Sensors)
                    .ThenInclude(s => s.MeasuringComponent)
                .Include(a => a.Sensors)
                     .ThenInclude(s => s.Unit)
                .Include(a => a.Sensors)
                     .ThenInclude(s => s.CalibrationFunctions)

                .FirstOrDefault(t => t.AnalyzerId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpGet("{id}/CurrentStatus", Name = "GetAnalyzerCurrentstatus")]
        public IActionResult GetCurrentStatus(long id)
        {
            var item = _context.Analyzers
                .FirstOrDefault(t => t.AnalyzerId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(new { StatusCode = 1 });
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Analyzer item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _context.Analyzers.Add(item);
            _context.SaveChanges();
            await hubContext.Clients.All.SendAsync("add");

            return CreatedAtRoute("GetAnalyzer", new { id = item.AnalyzerId }, item);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var item = _context.Analyzers.FirstOrDefault(t => t.AnalyzerId == id);
            if (item == null)
            {
                return NotFound();
            }
            _context.Analyzers.Remove(item);
            _context.SaveChanges();
            await hubContext.Clients.All.SendAsync("delete");
            return new NoContentResult();

        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] Analyzer item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.AnalyzerId)
            {
                return BadRequest();
            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
                await hubContext.Clients.All.SendAsync("update");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }

            }
            return NoContent();

        }
        private bool ItemExists(int id)
        {
            return _context.Analyzers.Any(f => f.AnalyzerId == id);
        }



    }

}
