﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

using SMECService.Models;
using SMECService.Data;
using Microsoft.AspNetCore.SignalR;
using SMECService.Hubs;

using Newtonsoft.Json;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    // [EnableCors("AllowAllOrigins")]
    public class FocusController : Controller
    {
        private readonly CEMSContext _context;
        private readonly IHubContext<DataHub> hubContext;

        public FocusController(CEMSContext context, IHubContext<DataHub> hub)
        {
            _context = context;
            hubContext = hub;
        }

        [HttpGet]
        public IEnumerable<Focus> GetAll()
        {
            return _context.Focus
            .Include(se => se.Series)
                .Include(f => f.Analyzers)
                    .ThenInclude(a => a.Sensors)
                        .ThenInclude(s => s.MeasuringComponent)
                .Include(f => f.Analyzers)
                    .ThenInclude(a => a.Sensors)
                         .ThenInclude(s => s.Unit)
                .Include(f => f.Analyzers)
                    .ThenInclude(a => a.Sensors)
                        .ThenInclude(s => s.CalibrationFunctions)
                .Include(f => f.Analyzers)
                         .ThenInclude(a => a.Sensors)
                 .ToList();
        }
        //GET : api/Focus/5
        [HttpGet("{id}", Name = "GetFocus")]
        public IActionResult GetById(long id)
        {
            var item = _context.Focus
            .Include(se => se.Series)
                .Include(f => f.Analyzers)
                    .ThenInclude(a => a.Sensors)
                        .ThenInclude(s => s.MeasuringComponent)
                .Include(f => f.Analyzers)
                    .ThenInclude(a => a.Sensors)
                         .ThenInclude(s => s.Unit)
                .Include(f => f.Analyzers)
                    .ThenInclude(a => a.Sensors)
                        .ThenInclude(s => s.CalibrationFunctions)
                .FirstOrDefault(t => t.FocusId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }


        [HttpGet("list")]
        public async Task<ActionResult> GetList([FromQuery]List<int> ids)
        {
            var list = new List<Focus>();

            foreach (var id in ids)
            {
                var items = await _context.Focus.FindAsync(id);
                list.Add(items);
            }

            return new ObjectResult(list);
        }

        [HttpGet("{id}/CurrentStatus", Name = "GetFocusCurrentstatus")]
        public IActionResult GetCurrentStatus(long id)
        {
            var item = _context.Focus
                .FirstOrDefault(t => t.FocusId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(new { StatusCode = 1 });
        }
        // POST : api/Focus/Create
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] Focus item)
        {
            if (item == null)
            {
                return BadRequest();
            }
            _context.Focus.Add(item);
            _context.SaveChanges();
            await hubContext.Clients.All.SendAsync("add", "add en el hub");

            return CreatedAtRoute("GetFocus", new { id = item.FocusId }, item);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var item = _context.Focus.FirstOrDefault(t => t.FocusId == id);
            if (item == null)
            {
                return NotFound();
            }
            _context.Focus.Remove(item);
            _context.SaveChanges();
            await hubContext.Clients.All.SendAsync("delete", "Delete en el hub");
            return new NoContentResult();

        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] Focus item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.FocusId)
            {
                return BadRequest();

            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {

                await _context.SaveChangesAsync();
                string json = JsonConvert.SerializeObject(item);
                await hubContext.Clients.All.SendAsync("update", json);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }

            }
            return NoContent();


        }
        private bool ItemExists(int id)
        {
            return _context.Focus.Any(f => f.FocusId == id);
        }


    }

}

