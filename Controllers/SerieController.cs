using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Http;
using SMECService.Models;
using SMECService.Data;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class SerieController : Controller
    {
        private readonly CEMSContext _context;

        private UserManager<ApplicationUser> _userManager;

        private readonly IHttpContextAccessor _httpContextAccessor;


        public SerieController(CEMSContext context, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;

        }

        [HttpGet]
        public IEnumerable<Serie> GetAll()
        {
            return _context.Series.ToList();
        }

        [HttpGet("{id}", Name = "GetConfig")]
        public IActionResult GetById(long id)
        {
            var item = _context.Series.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        //[Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrator")]
        public IActionResult Create([FromBody] Serie item)
        {
            Console.WriteLine("===> Administrator");
            if (item == null)
            {
                return BadRequest();
            }

            _context.Series.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetConfig", new { id = item.Id }, item);
        }

        //delete
        [HttpDelete("{id}")]
        //[Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrator")]
        public IActionResult Delete(long id)
        {
            Console.WriteLine("===> Administrator");
            var item = _context.Series.FirstOrDefault(s => s.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            _context.Series.Remove(item);
            _context.SaveChanges();
            return new NoContentResult();

            /*ApplicationUser user = await _userManager.FindByNameAsync("jonathan");
             var roles = await _userManager.GetRolesAsync(user);
             string currentRole = "";


             foreach (string role in roles)
             {
                 currentRole = role;
             }

             if (currentRole == "Administrator")
             {
                 // delete
             }
             else
             {
                 return new UnauthorizedResult();

             }*/

        }

        [HttpPatch("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] Serie item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.Id)
            {
                return BadRequest();

            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }

            }
            return NoContent();
        }
        private bool ItemExists(int id)
        {
            return _context.Series.Any(s => s.Id == id);
        }

    }


}
