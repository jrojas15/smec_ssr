using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SMECService.Models;
using SMECService.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;



namespace SMECService.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    public class SensorPerifericController : Controller
    {
        private readonly CEMSContext _context;

         public SensorPerifericController(CEMSContext context)
        {
            _context = context;
        }

        // Get CAlibrationFunctions
        [HttpGet]
        public IEnumerable<SensorPeriferic> GetAll()
        {
           return _context.SensorPeriferics
                    .Include(sp => sp.Periferic)
                        .ThenInclude(p => p.Sensor)
                    .ToList();
        }

        //Get by id
      /*  [HttpGet("{id}", Name="GetCalibrationFunction")]
        public IActionResult GetById(long id)
        {
            var item = _context.CalibrationFunctions
                .Include(c => c.Sensor)
                .FirstOrDefault(c => c.CalibrationFunctionId == id);
            if(item ==null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }*/

        //POST : api/calibrationfunction/create
       /* [HttpPost("Create")]
        public IActionResult Create([FromBody] CalibrationFunction item)
        {
            if(item == null){
                return BadRequest();
            }
            _context.CalibrationFunctions.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetCalibrationFunction", new {id = item.CalibrationFunctionId}, item);

        }*/

        //DELETE : api/calibrationfunction/1
       /* [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var item = _context.CalibrationFunctions.FirstOrDefault(t => t.CalibrationFunctionId == id);
            if(item == null){
                return NotFound();
            }

            _context.CalibrationFunctions.Remove(item);
            _context.SaveChanges();
            return new NoContentResult();
        }*/

        //UPDATE : api/calibrationfunction/1
       /* [HttpPatch("{id}")]
        public async Task <IActionResult> Update([FromRoute]int id,[FromBody] CalibrationFunction item)
        {
            if(!ModelState.IsValid){
                return BadRequest(ModelState);
            }
            if(id != item.CalibrationFunctionId){
                return BadRequest();
            }
            _context.Entry(item).State = EntityState.Modified;
            try {
                await _context.SaveChangesAsync();

            }catch(DbUpdateConcurrencyException){
                if(!ItemExists(id)){
                    return NotFound();
                }else {
                    throw;
                }
            }
            return NoContent();
        }
        private bool ItemExists(int id){
            return _context.CalibrationFunctions.Any(c => c.CalibrationFunctionId == id);
        }*/



        
      


         }
    
    
    }